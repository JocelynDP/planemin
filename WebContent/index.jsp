<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Empleados</title>
</head>
<body>
<h6>Registros de empleados</h6>
<h1>Listado de Cargos</h1>
	<table>
		<thead>
			<tr>
				<th>ID</th>
				<th>Nombre</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${cargos}" var="c">
				<tr>
					<td>${c.getIdCargo()}</td>
					<td>${c.getCargo()}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table><br/>
	<a  href="/Planilla_Empleado/empleados">Empleados</a>
</body>
</html>