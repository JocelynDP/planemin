<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html xmlns:th="https://www.thymeleaf.org">
<head>
<meta charset="ISO-8859-1" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<title>Listado</title>
</head>
<body>
	<div class="container">
		<h1>Listado de empleados</h1>
		<table border="3">
			<thead style="border: 1px solid black;">
				<tr>
					<th>C�digo</th>
					<th>Nombre completo</th>
					<th>Datos personales</th>
					<th>Documentos personales</th>
					<th>Datos laborales</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody style="border: 1px solid black;">
				<c:forEach items="${empleado}" var="e">
					<tr>
						<td>${e.getCodEmp()}</td>
						<td>${e.getPrimNomb()}${e.getSegApe()}</td>
						<td>F. Nacimiento: ${e.getFechaNacimiento()}
							<br/>Sexo: ${e.getSexoEmp()}
							<br/>Direcci�n: ${e.getDireccionEmp()}
							<br/>Estado civil: ${e.estadosCiviles.estadoCivil}
							<br/>Pa�s residencia: ${e.paises.pais}
							<br/>Tel�fono: ${e.getTelefonoPersonal()}
							<br/>Email: ${e.getEmailPersonal()} 
						</td>
						<td>DUI: ${e.getDuiEmp()} <br /> NIT: ${e.getNitEmp()}<br />AFP:
							${e.getAfpEmp()}<br />ISSS: ${e.getIsssEmp()}
						</td>
						<td>Email Empresa: ${e.getEmailEmpresa()} 
							<br/>Tel�fono empresa:${e.getTelefonoEmpresa()}
							<br/>Fecha de alta: ${e.getFechaAlta()}
							<br/>Estado en la empresa: ${e.isEstado()}
							<br/>Departamento asignado: ${e.departamentos.departamento}
						</td>
						<td></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<br />
		<a  class="btn btn-primary" href="redEmp" style="margin-left:50%;">Agregar Empleado</a>
	</div>
</body>
</html>