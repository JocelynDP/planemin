package com.planilla.utils;

import java.util.List;

public interface Dao<T> {
	public void create(T c);
    public void remove(T c);
    public void edit(T c);
    public T find(Object c);
    public List<T> findAll();
}
