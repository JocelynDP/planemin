
package com.planilla.utils;

import java.util.List;
import javax.persistence.EntityManager;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Elena
 */
public abstract class AbsFacade<T> {

	private Class<T> entityClass;

	public AbsFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public abstract SessionFactory getSessionFactory();

	@Transactional
	public void create(T entity) {
		getSessionFactory().getCurrentSession().save(entity);
	}
	
	@Transactional
	public void edit(T entity) {
		getSessionFactory().getCurrentSession().update(entity);
	}

	@Transactional
	public void remove(T entity) {
		getSessionFactory().getCurrentSession().delete(entity);
	}

	@Transactional
	public T find(Object id) {
		return getSessionFactory().getCurrentSession().find(entityClass, id);
	}
	
	@Transactional
	public List<T> findAll() {
		javax.persistence.criteria.CriteriaQuery cq = getSessionFactory().getCriteriaBuilder().createQuery();
		cq.select(cq.from(entityClass));
		return getSessionFactory().getCurrentSession().createQuery(cq).getResultList();
	}

}
