package com.planilla.utils;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.planilla.imp.CargoRep;
import com.planilla.imp.EmpleadoRep;

@Configuration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableWebMvc
@ComponentScan("com.planilla")
public class Configuracion {

	@Bean
	InternalResourceViewResolver vistas() {
		InternalResourceViewResolver resultado = new InternalResourceViewResolver();
		resultado.setPrefix("/");
		resultado.setSuffix(".jsp");
		return resultado;
	}
	
	/*Es el primer bean por el que se pasa, lo que se desea obtener es la base de datos
	 * de este m�todo es necesario pasarse al SessionFactory 
	 */
	@Bean(name = "datasource")
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://usam-sql.sv.cds:3306/bd_planilla?useSSL=false");
		dataSource.setUsername("kz");
		dataSource.setPassword("kzroot");
		return dataSource;
	}
	
	/*
	 * Necesita tomar el DataSource previamente creado para conectarse y crear las inyecciones o consultas
	 * de la base de datos (2)
	 */
	@Autowired
	@Bean("sessionFactory")
	public LocalSessionFactoryBean startSession() {
		LocalSessionFactoryBean fabrica = new LocalSessionFactoryBean();
		fabrica.setDataSource(dataSource());
		fabrica.setPackagesToScan("com.planilla.models");
		fabrica.setHibernateProperties(getHibernateProperties());
		return fabrica;
	}

	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager tx = new HibernateTransactionManager(sessionFactory);
		return tx;
	}

	private Properties getHibernateProperties() {
		Properties hibernateProperties = new Properties();
		// hibernateProperties.put("hibernate.show_sql", "true");
		hibernateProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		return hibernateProperties;
	}
	
	/*Se necesita obtener el SessionFactory que se necesita retornar en el m�todo del ManagerBean 
	 * para hacer las respectivas consultas (3)
	 * se necesita enviarlo al controlador. Accedemos a �l a trav�s del nombre del Bean
	 */
	@Autowired
	@Bean(name = "carImp")
	public CargoRep carImp(SessionFactory sessionFactory) {
		return new CargoRep(sessionFactory);
	}
	
	@Autowired
	@Bean(name = "empImp")
	public EmpleadoRep empImp(SessionFactory sessionFactory) {
		return new EmpleadoRep(sessionFactory);
	}	
}
