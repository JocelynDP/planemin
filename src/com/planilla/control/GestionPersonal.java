package com.planilla.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.planilla.imp.*;
import com.planilla.models.*;

@SuppressWarnings("unchecked")
@Controller
public class GestionPersonal {

	
	/*Al tener un "m�todo" con el datasource precargado y cuyo bean ha sido definido con un nombre,
	 *debemos agregar una etiqueta Qualifier con el nombre del Bean que ha sido definido en la
	 * 
	 */
	@Autowired
	@Qualifier("carImp")
	private CargoRep cargo;
	
	@Autowired
	@Qualifier("empImp")
	private EmpleadoRep empleado;
	
	@RequestMapping(value = "/listado", method = RequestMethod.GET)
	public ModelAndView verCargos() {
		ModelAndView mv = new ModelAndView("index");
		
		mv.addObject("cargos", cargo.findAll());
		
		return mv;
	}
	
	@RequestMapping(value = "/empleados",method = RequestMethod.GET)
	public ModelAndView verEmpleados() {
		ModelAndView mv = new ModelAndView("empleados");
		
		mv.addObject("empleado", empleado.findAll());
		
		return mv;
	}
	
	@RequestMapping(value = "/redEmp", method = RequestMethod.GET)
	public ModelAndView addEmpleado() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("view/registroEmpleado");
		return mv;
	}
}











