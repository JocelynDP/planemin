package com.planilla.imp;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.planilla.models.Paises;
import com.planilla.utils.AbsFacade;
import com.planilla.utils.Dao;

@Repository
public class PaisRep extends AbsFacade<Paises> implements Dao<Paises>{

	@Autowired
	private SessionFactory getSF;
	
	public PaisRep(SessionFactory sessionFactory) {
		super(Paises.class);
		this.getSF = sessionFactory;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return getSF;
	}

}
