package com.planilla.imp;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.planilla.models.Cargos;
import com.planilla.utils.AbsFacade;
import com.planilla.utils.Dao;

@Repository
@Transactional
public class CargoRep extends AbsFacade<Cargos> implements Dao<Cargos>{

	/*Necesitamos instanciar el valor del SessionFactory que se ha definido en la clase configuración
	 * esto se logra a través de la encapsulación de este en una variable privada
	 * luego se inserta como un parámetro en el constructor, y esto se usa para manejar los eventos del 
	 * controlador (4)
	 */
	private SessionFactory getSF;
	
	// Inyección por constructor
	public CargoRep(SessionFactory sessionFactory) {
		super(Cargos.class);
		this.getSF = sessionFactory;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return getSF;
	}

}
