package com.planilla.imp;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.planilla.models.Empleados;
import com.planilla.utils.AbsFacade;
import com.planilla.utils.Dao;

@Repository
public class EmpleadoRep extends AbsFacade<Empleados> implements Dao<Empleados>{
	
	@Autowired
	private SessionFactory getSF;
	
	public EmpleadoRep(SessionFactory sessionFactory) {
		super(Empleados.class);
		this.getSF = sessionFactory;
	}

	@Override
	public SessionFactory getSessionFactory() {
		return getSF;
	}
	
	@Transactional(readOnly = true)
	public List<Empleados> getEmpleados(){
		return null;
	}
}
